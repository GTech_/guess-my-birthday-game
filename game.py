from mimetypes import guess_extension
from random import randint
from tkinter.messagebox import YES

name = input("Whats is your name?")

for guess_number in range(1,6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Guess ", guess_number, ": ", name, " were you born on "
        , month_number, " / ", year_number, " ?") 

    response = input("yes or no?")
    if response == "yes":
        print("I win")
        exit()
    elif guess_number == 5:
        print("oh snap, you beat me, good game bro!")
    else:
        print("I was wrong, ill try again")

